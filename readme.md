[参考链接](https://blog.csdn.net/east196/category_11522700)

# Ubuntu配置ThingsBoard

环境介绍：

硬件：

```bash
处理器： Intel(R) Core(TM) i5-10210U CPU @ 1.60GHz 2.11 GHz
机带RAM： 16.0 GB (15.8 GB可用)
```

Ubuntu：VMware：

```
操作系统名称：Linux
发行版名称：Ubuntu
版本号：5.15.0-79-generic
编译版本号：#86~20.04.2
架构：x86_64
处理器数量：3个，原分配 1核心+2GB内存 ，不够后增加至 3核心+4GB内存
```

docker工具：

```
Docker version 24.0.6
```

## 一、ThingsBoard 介绍

ThingBoard可以分为四个核心模块：

```
设备管理、数据接入、规则引擎、部件面板
```

![](pictures/1.png)

ThingsBoard可用于:

```
设备管理，资产和客户并定义他们之间的关系。
基于设备和资产收集数据并进行可视化。
采集遥测数据并进行相关的事件处理进行警报响应。
基于远程RPC调用进行设备控制。
基于生命周期事件、REST API事件、RPC请求构建工作流。
基于动态设计和响应仪表板向你的客户提供设备或资产的遥测数据。
基于规则链自定义特定功能。
发布设备数据至第三方系统。
涵盖了各种常见的物联网需求，不常见的也可以通过配置和二次开发完美完成。
```

官网：

```
中文
http://www.ithingsboard.com/

英文
https://thingsboard.io/
```

## 二、安装

### 1、定义安装环境

```bash
vi docker-compose.yml
```

### 2、编写docker-compose.yml

zookeeper: https://baijiahao.baidu.com/s?id=1687337357484700521&wfr=spider&for=pc

kafka: https://zhuanlan.zhihu.com/p/163836793

thingsboard: https://blog.csdn.net/u013433591/article/details/127525310

```bash
version: '2.2'
services:
  zookeeper:
    restart: always
    image: "zookeeper:3.5"
    ports:
      - "2181:2181"
    environment:
      ZOO_MY_ID: 1
      ZOO_SERVERS: server.1=zookeeper:2888:3888;zookeeper:2181
  kafka:
    restart: always
    image: wurstmeister/kafka
    depends_on:
      - zookeeper
    ports:
      - "9092:9092"
    environment:
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_LISTENERS: INSIDE://:9093,OUTSIDE://:9092
      KAFKA_ADVERTISED_LISTENERS: INSIDE://:9093,OUTSIDE://kafka:9092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: INSIDE
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
  mytb:
    restart: always
    image: "thingsboard/tb-postgres"
    depends_on:
      - kafka
    ports:
      - "9090:9090"
      - "1883:1883"
      - "5683:5683/udp"
    environment:
      TB_QUEUE_TYPE: kafka
      TB_KAFKA_SERVERS: kafka:9092
    volumes:
      - /data/.mytb-data:/data
      - /data/.mytb-logs:/var/log/thingsboard

#  restart: always - 在系统重新启动的情况下自动启动ThingsBoard在出现故障的情况下自动重新启动ThingsBoard。
#  image: thingsboard/tb-postgres - docker镜像也可以是thingsboard/tb-cassandra或thingsboard/tb
#  "PORT:PORT" 顺序是 “本地端口:Docker容器内端口”
#  8080:9090 - 将本地端口9090转发至Docker容器内的HTTP端口9090
#  1883:1883 - 将本地端口1883转发至Docker容器内的MQTT端口1883
#  5683:5683 - 将本地端口5683转发至Docker容器内的COAP端口5683
#  "DIR:DIR" 顺序是 “本地主机目录:Docker容器内目录”
#  /data/.mytb-data:/data - 将主机的目录/data/.mytb-data挂载到ThingsBoard数据目录
#  /data/.mytb-logs:/var/log/thingsboard - 将主机的目录/data/.mytb-logs挂载到ThingsBoard日志目录
```

### 3、授予目录权限

```bash
mkdir -p /data/.mytb-data && sudo chown -R 799:799 /data/.mytb-data
mkdir -p /data/.mytb-logs && sudo chown -R 799:799 /data/.mytb-logs
```

### 4、启动容器

```bash
docker-compose pull
docker-compose up
```

后台启动：

```bash
docker-compose up -d
```

## 三、配置

### 1、登录：

```bash
http://localhost:9090
```

默认用户名/密码如下：

```bash
系统管理员: sysadmin@thingsboard.org / sysadmin
租户管理员: tenant@thingsboard.org / tenant
客户: customer@thingsboard.org / customer
```

![](pictures/2.png)

系统管理员登陆后：
![](pictures/3.png)

租户管理员登陆后：
![](pictures/5.png)

客户登陆后：
![](pictures/6.png)

```
点击账户 -> 安全 -> 修改密码。

管理员、租户、客户关系：
```

![](pictures/4.png)

### 2、遥测设备

![](pictures/7.png)

```
遥测也就是我们常说的设备测量状态，比如温度计的温度，灯的亮度等等，由于是新设备，所以是没有遥测值的，我们可以用http协议发一个。
```

### 3、模拟发送遥测值

#### 3.1、获取对接设备的访问令牌

使用默认令牌
![](pictures/8.png)

#### 3.2、使用curl发送HTTP请求

如：

```bash
curl -v -X POST -d '{"turn":"1","light":"90"}' http://http://192.168.220.128/:9090/api/v1/A1_TEST_TOKEN/telemetry --header "Content-Type:application/json"

向指定 URL 发送一个 HTTP POST 请求。根据命令的参数，向 http://192.168.220.128:9090/api/v1/A1_TEST_TOKEN/telemetry 发送了一个名为 “turn” 的参数值为 “1”、名为 “light” 的参数值为 “90” 的 JSON 数据。并在请求头中指定了 “Content-Type” 为 “application/json”。
```

结果：

![](pictures/9.png)


## 四、添加设备

### 1、添加设备

#### 添加设备：

![](pictures/10.png)

#### 分配设备给用户：

![](pictures/11.png)

#### MQTT X测试：

![](pictures/12.png)

MQTT X：https://blog.csdn.net/m0_46155417/article/details/131251802

#### http测试:

![](pictures/13.png)

```
结果成功.
```

## 五、设备/遥测/展示

### 1、设备页面

![](pictures/14.png)

```
属性：基础信息，比较稳定
遥测：需要测量的状态信息
警告：设备或者设备监控的状态出现问题
事件：发生在设备上的事件
关联：设备属于谁，在哪个资产上等等
审计日志：谁在平台上对设备做了什么操作
其中属性是基础，遥测是核心。
```

### 2、属性

```
ThingsBoard能够给实体分配自定义属性并进行管理。
属性 代表设备基本信息, 以key-value格式存在, 可以与IoT设备无缝兼容。
属性分客户端属性，服务端属性和共享属性。
```

```
客户端属性
```

![](pictures/15.png)

```
服务端属性
```

![](pictures/16.png)

```
共享属性
```

![](pictures/17.png)

### 3、遥测

```
物联网的核心目的之一就是通过传感器采集相应的遥测数据上传。对此，ThingsBoard 提供了大量与遥测数据操作相关的功能：
```

```
采集 使用MQTT, CoAP或者HTTP协议采集设备数据。
存储 在Cassandra（高效、可扩展、能容错的NoSQL数据库）中存储时序数据。
查询 查询最新时序数据值，或查询特定时间段内的所有数据。
订阅 使用websockets订阅数据更新(用于可视化或实时分析)。
可视化 使用可配置和可配置的小部件以及仪表盘可视化时序数据。
过滤和分析 使用灵活的规则引擎过滤和分析数据(/docs/user-guide/rule-engine/)。
事件警报 根据采集的数据触发事件警报。
数据传输 过规则引擎节点实现与外部数据交互（例如Kafka或RabbitMQ规则节点）
```

![](pictures/18.png)

```
接下来模拟：
  开关
  亮度
  电量
  位置信息
```

### 4、设备模拟

```
想要连接设备，首先要搞清楚下载上传数据的API 以及 设备和平台连接的API。

设备API
和云平台进行通信的主要api是device api,
部署好平台就直接可以在浏览器中访问http://IP:9090/swagger-ui/#/device-api-controller
```

### 5、仪表盘

#### 添加电量：

![](pictures/19.png)

![](pictures/20.png)

![](pictures/21.png)

![](pictures/22.png)

![](pictures/23.png)

![](pictures/24.png)

![](pictures/25.png)

![](pictures/26.png)

![](pictures/27.png)

```
设置控件参数
```

![](pictures/28.png)

![](pictures/29.png)

![](pictures/30.png)

![](pictures/30.png)

#### 添加 RPC（远程过程控制） 控件

##### 控制开关：

![](pictures/31.png)

![](pictures/32.png)

![](pictures/33.png)

![](pictures/34.png)

![](pictures/35.png)

```
编写python程序模拟开关：
```

```python
...
```

##### 控制亮度：

![](pictures/36.png)

![](pictures/37.png)

![](pictures/38.png)

```
编写python程序模拟开关：
```

```python
...
```

![](pictures/39.png)

#### 添加 地图 控件

```
地图显示的核心是位置信息，也就是我们常听到的坐标，

坐标系是坐标产生的参考背景，常见坐标系如下：

GCJ-02 高德地图、腾讯地图以及谷歌中国区地图使用的是GCJ-02坐标系
BD-09 百度地图使用的是BD-09坐标系
WGS-84 底层接口 (HTML5 Geolocation或ios、安卓API)通过GPS设备获取的坐标使用的是WGS-84坐标系
坐标是在坐标系上的位置点，一般用经纬度表示：

longitude 经度
latitude 纬度
```
```
添加部件
像往常一样，我们先找到地图部件包：
```

![](pictures/40.png)

```
选用腾讯地图
```

![](pictures/41.png)

![](pictures/42.png)

```
设置控件参数 
```

![](pictures/43.png)

```
修改Marker图标：
```

![](pictures/44.png)

![](pictures/45.png)

![](pictures/46.png)

![](pictures/47.png)







































